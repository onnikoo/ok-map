# OK Map

## About
Simple custom Google Map plugin for Wordpress created by [Pasi Heiskanen](https://www.onnikoodi.fi).


### Installing, settings and customizing

* Install the plugin normally
* [Get API key](https://developers.google.com/maps/documentation/javascript/get-api-key#types-of-api-keys)
* Enter the API key in "Settings >> Ok Map"
* To change the looks create a javascript style array using [Snazzymaps](https://snazzymaps.com/) or similar online service. The style can be entered in args styles array in ok-map.js.
* Replace /images/pin.png to your likings


### Usage

* Shortcode: [ok_map lat="61.0000" lng="25.000" zoom="14" class="css-class" title="Header" info="Content here"]
* Widget available as well
