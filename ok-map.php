<?php 
/*
 *	Plugin Name: OK Map
 *	Plugin URI: https://bitbucket.org/onnikoo/ok-map
 *	Description: Simple custom Google Map.
 *	Version: 1.0.1
 *	Author: Pasi Heiskanen
 *	Author URI: https://www.onnikoodi.fi
 *	License: GPL2
 *
*/

// Allow plugin to be easily translated
add_action( 'plugins_loaded', 'ok_map_textdomain' );
/**
 * Load plugin textdomain.
 *
 * @since 1.0.0
 */
function ok_map_textdomain() {
  load_plugin_textdomain( 'ok-map', true, basename( dirname( __FILE__ ) ) . '/languages' ); 
}

$plugin_dir = plugin_dir_path( __FILE__ );
$plugin_url = WP_PLUGIN_URL . '/ok-map';


// OPTIONS PAGE
require_once( $plugin_dir . '/includes/ok-map-options-page-functions.php');

// CLASSES
require_once( $plugin_dir . '/includes/ok-map-class.php');

// Widget
require_once( $plugin_dir . '/includes/ok-map-widget-class.php');

// SHORTCODES
require_once( $plugin_dir . '/includes/ok-map-shortcode.php');


//Add plugin settings link to Plugins page
function okmap_add_settings_link( $links ) {
    $settings_link = '<a href="options-general.php?page=okmap_options">' . __( 'Settings' ) . '</a>';
    array_push( $links, $settings_link );
  	return $links;
}
$plugin = plugin_basename( __FILE__ );
add_filter( "plugin_action_links_$plugin", 'okmap_add_settings_link' );

// SCRIPTS AND STYLES
function okmap_frontend_scripts_and_styles() {

	$options = get_option('okmap_options');
	$okmap_apikey = strip_tags($options['okmap_apikey']);
	if(strlen($okmap_apikey)) {
		$okmap_scripturl = 'https://maps.googleapis.com/maps/api/js?key='.$okmap_apikey;
		// https://developers.google.com/maps/documentation/javascript/get-api-key The key is set in "Settings >> Ok Map" page
	  wp_enqueue_script( 'googlemap-js', $okmap_scripturl, array( 'jquery' ), '', true );		
		wp_enqueue_script( 'okmap_frontend_js', plugins_url( 'ok-map/js/ok-map.js' ), array('jquery'), '', true );
		// wp_enqueue_script( 'okmap_frontend_js', plugins_url( 'ok-map/js/ok-map.min.js' ), array('jquery'), '', true );
		wp_enqueue_style( 'okmap_frontend_css', plugins_url( 'ok-map/styles/ok-map.css' ) );
	}

}

add_action( 'wp_enqueue_scripts', 'okmap_frontend_scripts_and_styles' );
