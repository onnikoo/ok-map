<?php // OHJEEET:
// https://carlalexander.ca/designing-class-generate-wordpress-html-content/

/* ADDING WIDGET */
class okMap_Widget extends WP_Widget {

	function __construct() {
		// Instantiate the parent object
		parent::__construct( 
			'ok-map', 
			'OK Map',
			array(
        'description' => 'Custom Google Map'
      )
	 	);
	}

	function widget( $args, $instance ) {

		// Widget output
		extract( $args );
		$title = apply_filters( 'widget_title', $instance['title'] );
		$lat = $instance['lat'];
		$lng = $instance['lng'];
		$info_header = $instance['info_header'];
		$info_content = $instance['info_content'];
		$zoom = $instance['zoom'];
		$class = $instance['class'];

		if( $lat && $lng) {
	  	require( 'ok-map-template.php');
		} else {
			echo '<!-- Sijaintitietoa ei ole -->';
		}

	}

	function update( $new_instance, $old_instance ) {
		// Save widget options
		$instance = $old_instance;
		$instance['title'] = strip_tags($new_instance['title']);
		$instance['lat'] = strip_tags($new_instance['lat']);
		$instance['lng'] = strip_tags($new_instance['lng']);
		$instance['info_header'] = strip_tags($new_instance['info_header']);
		$instance['info_content'] = strip_tags($new_instance['info_content']);
		$instance['zoom'] = strip_tags($new_instance['zoom']);
		$instance['class'] = strip_tags($new_instance['class']);
		return $instance;
	}

	function form( $instance ) {
		// Output admin widget options form
		$title = esc_attr( $instance['title'] );
		$lat = esc_attr( $instance['lat'] );
		$lng = esc_attr( $instance['lng'] );
		$info_header = esc_attr( $instance['info_header'] );
		$info_content = esc_attr( $instance['info_content'] );
		$zoom = esc_attr( $instance['zoom'] );
		$class = esc_attr( $instance['class'] );
		require( 'ok-map-widget-fields.php');
	}

}

function ok_map_register_widgets() {
	register_widget( 'okMap_Widget' );
}

add_action( 'widgets_init', 'ok_map_register_widgets' );
