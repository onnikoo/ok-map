<p>
  <label><?php esc_attr_e( 'Latitude *', 'ok-map' ); ?></label> 
  <input class="widefat" name="<?php echo $this->get_field_name('lat'); ?>" type="text" value="<?php echo $lat; ?>" />
</p>

<p>
  <label><?php esc_attr_e( 'Longitude *', 'ok-map' ); ?></label> 
  <input class="widefat" name="<?php echo $this->get_field_name('lng'); ?>" type="text" value="<?php echo $lng; ?>" />
</p>

<p>
  <label><?php esc_attr_e( 'Infobox header', 'ok-map' ); ?></label> 
  <input class="widefat" name="<?php echo $this->get_field_name('info_header'); ?>" type="text" value="<?php echo $info_header; ?>" />
</p>

<p>
  <label><?php esc_attr_e( 'Infobox content', 'ok-map' ); ?></label> 
  <input class="widefat" name="<?php echo $this->get_field_name('info_content'); ?>" type="text" value="<?php echo $info_content; ?>" />
</p>

<p>
  <label><?php esc_attr_e( 'Zoom', 'ok-map' ); ?></label> 
  <input class="widefat" name="<?php echo $this->get_field_name('zoom'); ?>" type="text" value="<?php echo $zoom; ?>" />
</p>

<p>
  <label><?php esc_attr_e( 'Class', 'ok-map' ); ?>Class</label> 
  <input class="widefat" name="<?php echo $this->get_field_name('class'); ?>" type="text" value="<?php echo $class; ?>" />
</p>