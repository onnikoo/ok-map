<?php 
  $s = strtoupper(md5(uniqid(rand(),true)));
  $uniqID = substr($s,0,6);
  $mapID = 'map__'.$uniqID;
  $pin =  WP_PLUGIN_URL . '/ok-map/images/pin.png';

  if(!wp_script_is('googlemap-js','enqueued')) {
		wp_enqueue_script('googlemap-js');
	}
?>

<?php if( isset($before_widget) ) {	echo $before_widget; } ?>
<div id="<?php echo $mapID; ?>" class="map <?php echo $class; ?>">
	<div 	class="map__map <?php echo $class; ?>__inner"
				data-zoom="<?php echo $zoom;?>">
		<div 	class="marker"
					data-lat="<?php echo $lat;?>"
					data-lng="<?php echo $lng;?>"
					data-pin="<?php echo $pin;?>"><?php if( $info_header || $info_content ): ?><div class="map__info"><?php  if( $info_header ): ?><h4><?php echo $info_header; ?></h4><?php endif; ?><?php  if( $info_content ): ?><p><?php echo $info_content; ?></p><?php endif; ?></div><?php endif; ?></div>
	</div>
</div>
<?php if( isset($after_widget) ) { echo $after_widget; } ?>
