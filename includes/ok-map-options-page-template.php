<div class="wrap">

	<div id="icon-options-general" class="icon32"></div>
	<h1><?php esc_attr_e( 'OK Map - Custom Google Map', 'ok-map' ); ?></h1>

	<div id="poststuff">
		<div id="post-body" class="metabox-holder columns-1">

			<!-- main content -->
			<div id="post-body-content">
				<div class="meta-box-sortables ui-sortable">


					<div class="postbox">

						<div class="handlediv" title="Click to toggle"><br></div>
						<!-- Toggle -->

						<h2 class="handle"><span><?php esc_attr_e( 'Settings', 'ok-map' ); ?></span>
						</h2>

						<div class="inside">

							<p><?php esc_attr_e( 'Get the API key using these instructions', 'ok-map' ); ?> https://developers.google.com/maps/documentation/javascript/get-api-key#types-of-api-keys</p>
							<form name="okmap_apikey_form" method="post" action="">

								<input type="hidden" name="okmap_form_submitted" value="Y" />

								<table class="form-table">

									<tr valign="top">
										<td>
											<label for="okmap_apikey">Google API key</label>
										</td>
										<td>
											<input type="text" value="<?php echo $okmap_apikey; ?>" name="okmap_apikey" id="okmap_apikey" class="regular-text" />
										</td>
									</tr>

									<tr valign="top">
										<td> </td>
										<td><input class="button-primary" type="submit" name="okmap_apikey_submit" value="Save" /></td>
									</tr>

								</table>

							</form>
						</div>
						<!-- .inside -->

					</div>
					<!-- .postbox -->

					<div class="postbox">
						<div class="handlediv" title="Click to toggle"><br></div>
						<!-- Toggle -->

						<h2 class="handle"><span><?php esc_attr_e( 'Shortcode', 'ok-map' ); ?></span>
						</h2>

						<div class="inside">
							<p>[ok_map lat="61.0000" lng="25.000" zoom="14" class="css-class" title="Header" info="Content here"]</p>
							<p><?php esc_attr_e( 'Only lat and lng are required', 'ok-map' ); ?></p>
							<p><?php esc_attr_e( 'Info will show up when the pin is clicked.', 'ok-map' ); ?></p>
						</div>

					</div>


				</div>
				<!-- .meta-box-sortables .ui-sortable -->
			</div>
			<!-- post-body-content -->

		</div>
		<!-- #post-body .metabox-holder .columns-1 -->
		<br class="clear">
	</div>
	<!-- #poststuff -->

</div> <!-- .wrap -->