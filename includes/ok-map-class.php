<?php // OHJEEET:
// https://carlalexander.ca/designing-class-generate-wordpress-html-content/

class okMap {

  /**
   * Path to default template
   *
   * @var string
   */
  private $default_template_path;

  /**
   * name of the filter editing template path
   * 
   * @var string
   */
  private $filter_name;

  /**
  * Naem of the template used in `get_query_template` function
  * so you can use own template in theme root folder
  * 
  * @var string
  */
  private $query_template_name;

  /**
  * Constructor.
  *
  * @param string $default_template_path
  * @param string $filter_name
  * @param string $query_template_name
  */
  public function __construct($default_template_path, $filter_name, $query_template_name)
  {
    $this->default_template_path = $default_template_path;
    $this->filter_name = $filter_name;
    $this->query_template_name = $query_template_name;
  }

  /**
   * Generates the HTML code (event) based on lat ja lng values.
   *
   * @param $lat
   * @param $lng
   *
   * @return string
   */

  public function okmap_write_map($lat,$lng,$zoom,$class,$info_header,$info_content) {

    // määritetään template-filu
    $template_path = $this->get_template_path();

    // jos ei, niin ilmoitetaan siitä
    if (!is_readable($template_path)) {
      return sprintf('<!-- Could not read "%s" file -->', $template_path);
    }

    ob_start();
   	include $template_path;
    return ob_get_clean();
    
  }

  /**
  * Returns  the path of PHP-template which okmap_write_map uses.
  *
  * @return string
  */
  private function get_template_path(){

    // checking if there is a template file in theme root folder...
    $template_path = get_query_template($this->query_template_name);

    // ... if not, use template file of the plugin
    if (empty($template_path)) {
      $template_path = $this->default_template_path;
    }
    // ... so developers can use filters...
    // https://carlalexander.ca/wordpress-adventurous-plugin-api/
    return apply_filters($this->filter_name, $template_path);

  }

} // end class





