<?php 

/* SHORTCODE */
function ok_map_shortcode( $atts ) {

	$attributes = shortcode_atts( array(
		'lat' 	=> '0',
		'lng' 	=> '0',
		'zoom'	=> '14',
		'class'	=> 'normal',
		'title' => false,
		'info' => false,

	), $atts );

	$lat = $attributes['lat'];
	$lng = $attributes['lng'];
	$zoom = $attributes['zoom'];
	$class = $attributes['class'];
	$info_header = $attributes['title'];
	$info_content = $attributes['info'];

	if( $lat && $lng ) {
		$okmap_writer = new okMap(
	    plugin_dir_path( __DIR__ ) . 'includes/ok-map-template.php', // path to the template of plugin
	    'ok-map-filter', // name of the filter
	    'ok-map-template' // template name that is used in theme root folder
	  );
  	return $okmap_writer->okmap_write_map($lat,$lng,$zoom,$class,$info_header,$info_content);
	} else {
		return sprintf('<!-- No location data or the data is corrupt -->');
	}

}

add_shortcode( 'ok_map', 'ok_map_shortcode' );
