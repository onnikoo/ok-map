<?php 

$plugin_url = WP_PLUGIN_URL . '/ok-map';
$options = array();

/*
* Add a link to our plugin in the admin menu
* under 'Settings > OK Map'
*/
function okmap_menu() {

	/*
	 * Use the add_options_page function
	 * add_options_page( $page_title, $menu_title, $capability, $menu_slug, $function )
	 *
	*/

	add_options_page(
		'OK Map',
		'OK Map',
		'manage_options',
		'okmap_options',
		'okmap_options_page'
	);

}
add_action( 'admin_menu', 'okmap_menu');


function okmap_options_page() {

	if( !current_user_can( 'manage_options' ) ) {
		wp_die( 'Ei lupaa muokata tätä sivua');
	}

	global $plugin_url;
	global $options;

	if ( isset( $_POST['okmap_form_submitted'] ) ) {
		$hidden_field = esc_html($_POST['okmap_form_submitted'] );
		if( $hidden_field == 'Y' ) {
			$okmap_apikey = esc_html( $_POST['okmap_apikey']);
			$options['okmap_apikey'] 		= $okmap_apikey;
			$options['last_updated']   	= time();
			update_option( 'okmap_options', $options );
		}
	}

	$options = get_option( 'okmap_options');
	
	if ( $options != '' ) {
		$okmap_apikey = $options['okmap_apikey'];
	}

	require( 'ok-map-options-page-template.php' );

}

