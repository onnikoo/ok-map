var okMap = {}; // global variable for all jQuery modules

$(document).ready(function($){
	okMap.googlemap.init();
});

(function() {

// https://developers.google.com/maps/documentation/javascript/get-api-key#types-of-api-keys

    // create empty object in the global okMap var, don't forget to add the init call in the sources.js!
    okMap.googlemap = {};

    var selectedInfoWindow; // Tarvitaan html-infoikkunoita avatessa

    // call any functions to be trigger on dom ready
    okMap.googlemap.init = function(){

      $('.map__map').each(function(){
        okMap.googlemap.renderMap( $(this) );
      });

    };

    okMap.googlemap.renderMap = function($el){

      // markers
      var $markers = $el.find('.marker');
      var $zoomi = $el.attr('data-zoom');
      var zoomInt = parseInt($zoomi);

      // ARGS
      var args = {
          zoom    			: zoomInt,
          maxZoom 			: 19,
          minZoom 	    : 8,
          streetViewControl: true,
          mapTypeControl: false,
          scrollwheel 	: false,
          draggable			: true,
          center      	: new google.maps.LatLng(0, 0),
          styles        : [ /// Snazzymaps script here


    {
        "featureType": "administrative",
        "elementType": "all",
        "stylers": [
            {
                "saturation": "-100"
            }
        ]
    },
    {
        "featureType": "administrative.province",
        "elementType": "all",
        "stylers": [
            {
                "visibility": "off"
            }
        ]
    },
    {
        "featureType": "landscape",
        "elementType": "all",
        "stylers": [
            {
                "saturation": -100
            },
            {
                "lightness": 65
            },
            {
                "visibility": "on"
            }
        ]
    },
    {
        "featureType": "poi",
        "elementType": "all",
        "stylers": [
            {
                "saturation": -100
            },
            {
                "lightness": "50"
            },
            {
                "visibility": "simplified"
            }
        ]
    },
    {
        "featureType": "road",
        "elementType": "all",
        "stylers": [
            {
                "saturation": "-100"
            }
        ]
    },
    {
        "featureType": "road.highway",
        "elementType": "all",
        "stylers": [
            {
                "visibility": "simplified"
            }
        ]
    },
    {
        "featureType": "road.arterial",
        "elementType": "all",
        "stylers": [
            {
                "lightness": "30"
            }
        ]
    },
    {
        "featureType": "road.local",
        "elementType": "all",
        "stylers": [
            {
                "lightness": "40"
            }
        ]
    },
    {
        "featureType": "transit",
        "elementType": "all",
        "stylers": [
            {
                "saturation": -100
            },
            {
                "visibility": "simplified"
            }
        ]
    },
    {
        "featureType": "water",
        "elementType": "geometry",
        "stylers": [
            {
                "hue": "#ffff00"
            },
            {
                "lightness": -25
            },
            {
                "saturation": -97
            }
        ]
    },
    {
        "featureType": "water",
        "elementType": "labels",
        "stylers": [
            {
                "lightness": -25
            },
            {
                "saturation": -100
            }
        ]
    }
]
        };
			// END ARGS

      // create map
      var map = new google.maps.Map( $el[0], args);

      // add a markers reference
      map.markers = [];

      // add markers
      $markers.each(function(){
      	okMap.googlemap.addMarker( $(this), map );
      });

      // center map
      okMap.googlemap.centerMap( map, zoomInt );

    }

   //  okMap.googlemap.listen = function( map ) {
  	// // https://developers.google.com/maps/documentation/javascript/events

	  //   map.addListener('click', function() {
	  //     window.setTimeout(function() {
	  //       $('.mapmodule .jspan').each(function(){
	  //       	$(this).addClass('jspan__small');
	  //       });
	  //     }, 400);
	  //   });

	  //   map.addListener('dragend', function() {
	  //     window.setTimeout(function() {
	  //       $('.mapmodule .jspan').each(function(){
	  //       	$(this).addClass('jspan__small');
	  //       });
	  //     }, 400);
	  //   });

	  //   map.addListener('zoom_changed', function() {
	  //     window.setTimeout(function() {
	  //       $('.mapmodule .jspan').each(function(){
	  //       	$(this).addClass('jspan__small');
	  //       });
	  //     }, 400);
	  //   });

	  //   map.addListener('idle', function() {
   //      $('.mapmodule__container').each(function(){
   //      	$(this).addClass('mapmodule__container--loaded');
   //      });
	  //   });

	  // }

    okMap.googlemap.addMarker = function( $marker, map ) {

      // vars
      var lati = parseFloat($marker.attr('data-lat'));
      var lngi = parseFloat($marker.attr('data-lng'));
      var latlng = new google.maps.LatLng( lati, lngi );
      var markerimage = $marker.attr('data-pin');

      // create marker
      var marker = new google.maps.Marker({
        position    : latlng,
        icon        : markerimage,
        map         : map,
        clickableIcons: false,
        animation   : google.maps.Animation.DROP,
      });

      // add to array
      map.markers.push( marker );

      // if marker contains HTML, add it to an infoWindow
      if( $marker.html() ) {
        // create info window
        var infowindow = new google.maps.InfoWindow({
          content     : $marker.html()
        });

        selectedInfoWindow = new google.maps.InfoWindow({
          content     : $marker.html()
        });

        // marker click listener
        google.maps.event.addListener(marker, 'click', function() {

          // Tää ei sulje edellistä ikkunaa
          infowindow.open(map, this);

          // Check if there some info window selected and if is opened then close it
          if (selectedInfoWindow !== null && selectedInfoWindow.getMap() !== null) {
            selectedInfoWindow.close();
            //If the clicked window is the selected window, deselect it and return
            if (selectedInfoWindow === infowindow) {
              selectedInfoWindow = null;
              return;
            }
          }
          //If arrive here, that mean you should open the new info window
          //because is different from the selected
          selectedInfoWindow = infowindow;
          selectedInfoWindow.open(map, this);

        });
      }

    }

    okMap.googlemap.centerMap = function( map, zoomInt ) {

     // vars
      var bounds = new google.maps.LatLngBounds();

      // loop through all markers and create bounds
      $.each( map.markers, function( i, marker ){
          var latlng = new google.maps.LatLng( marker.position.lat(), marker.position.lng() );
          bounds.extend( latlng );
      });

      // only 1 marker?
      if( map.markers.length === 1 )
      {
          // set center of map
          map.setCenter( bounds.getCenter() );
          map.setZoom( zoomInt );
      }
      else
      {
          // fit to bounds
          // map.fitBounds( bounds );

          var zoomChangeBoundsListener =
              google.maps.event.addListener(map, 'bounds_changed', function(event) {
                  google.maps.event.removeListener(zoomChangeBoundsListener);
                  map.setZoom( Math.min( 15, zoomInt ) );
              });
          map.fitBounds( bounds );

      }

    }

})();
